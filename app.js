$(document).ready(function(){
	var item = {
		// name: 'Classic Tee',
		// imgSrc: 'assets/classic-tee.webp',
		// price: 75,
		// desc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur pariatur iure impedit aliquid totam dignissimos ea sunt provident itaque delectus fugiat, iusto illum voluptatum minima tempora, voluptates, ullam mollitia labore!',
		size: '',
	}

	var cart = {
		itemsTotalCount: 0,
		s: 0,
		m: 0,
		l: 0
		// totalPrice: 0,
	}

	console.log(item);
	$('.cart-block').on("click",function() {
		$(this).toggleClass('open');
	});
	
	$('.item-options .sizes li').on("click",function() {
		$(this).siblings().removeClass('selected');
		$(this).addClass('selected');
		item.size = $(this).find('span').html().toLowerCase();
		console.log(item);
	});


	$('.btn-addToCart').on("click", function(){
		if (item.size == '') {
			$('.alert').html('Please select your preferred Size').addClass('alert-danger');
			$('.alert').css('display','block');
			setTimeout(function(){
				$('.alert').hide();
			}, 3000);

		} else {
			if (item.size == 's') {
				cart.s = cart.s + 1;
				sumofcart();
				updateCart();
				$('.cart-item-size-s .cart-item-quantity').html(cart.s)
			}
			if (item.size == 'm') {
				cart.m = cart.m + 1;
				sumofcart();
				updateCart();
				$('.cart-item-size-m .cart-item-quantity').html(cart.m);
			}
			if (item.size == 'l') {
				cart.l = cart.l + 1;
				sumofcart();
				updateCart();
				$('.cart-item-size-l .cart-item-quantity').html(cart.l);
			}
			
		}
	});



	function sumofcart() {
		cart.itemsTotalCount = cart.s + cart.m + cart.l;
		console.log(cart);
	}

	function updateCart() {
		$('.items-count').html('('+cart.itemsTotalCount+')');
	}
});